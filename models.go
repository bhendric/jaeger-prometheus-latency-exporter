package main

type JaegerSpan struct {
	TraceID         string
	SpanID          string
	Flags           int
	OperationName   string
	References      []JaegerReference
	StartTime       uint64
	StartTimeMillis uint64
	EndTime         uint64 // EndTime is not returned by jaeger. This is something that is calculated in our business logic from the startTime + duration
	Duration        uint64
	Tags            []JaegerTagField
	Process         JaegerProcess
}

type JaegerReference struct {
	RefType string
	TraceID string
	SpanID  string
}

type JaegerTagField struct {
	Key   string
	Type  string
	Value string
}

type JaegerLog struct {
	Timestamp uint64
	Fields    []JaegerTagField
}

type JaegerProcess struct {
	ServiceName string
	Tags        []JaegerTagField
}
