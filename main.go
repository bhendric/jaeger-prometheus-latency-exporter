package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"time"

	"github.com/olivere/elastic/v7"
)

const startService = "frontend"
const startOperation = "HTTP GET /dispatch"

const endService = "route"
const endOperation = "HTTP GET /route"

const ipAddress = ""

var lastSuccessfulTraceTimestamp = 0

func main() {

	// Starting with elastic.v5, you must pass a context to execute each service
	ctx := context.Background()

	// Obtain a client and connect to the default Elasticsearch installation
	// on 127.0.0.1:9200. Of course you can configure your client to connect
	// to other hosts and configure it in various other ways.
	client, err := elastic.NewClient(
		elastic.SetSniff(false),
		elastic.SetURL("http://localhost:9200"),
	)
	if err != nil {
		// Handle error
		panic(err)
	}

	for range time.Tick(10 * time.Second) {
		fmt.Println("Starting new latency export")
		// Create the search query

		// Only fetch traces that have started more than 5 seconds ago to try minimize having partially finished traces
		rangeQuery := elastic.NewRangeQuery("startTime").Gt(lastSuccessfulTraceTimestamp).Lt(time.Now().UnixMicro() - 5_000_000)

		// Create a query when searching for tags. For this, we need to find a tag entry which simultaneously has the correct key and value combo in the list of tags
		// Since the tags is also an array, the bool query needs to be performed as a nested query on the list of tags
		ipTagKeyQuery := elastic.NewTermQuery("process.tags.key", "ip")
		ipTagValueQuery := elastic.NewTermQuery("process.tags.value", ipAddress)
		ipAddressBoolQuery := elastic.NewBoolQuery().Must(ipTagKeyQuery, ipTagValueQuery)
		nestedTagQuery := elastic.NewNestedQuery("process.tags", ipAddressBoolQuery)

		/****** Start span querying *****/
		var mustStartQueries []elastic.Query
		// Search for spans with the correct service and operation name
		startServiceTermQuery := elastic.NewTermQuery("process.serviceName", startService)
		startOperationTermQuery := elastic.NewTermQuery("operationName", startOperation)

		// Add the queries to the must clause of the upcoming bool query
		mustStartQueries = append(mustStartQueries, startServiceTermQuery)
		mustStartQueries = append(mustStartQueries, startOperationTermQuery)

		// If we already did a query, search only for new spans
		if lastSuccessfulTraceTimestamp > 0 {
			mustStartQueries = append(mustStartQueries, rangeQuery)
		}

		// Search on the operation tag fiels if value is given
		if ipAddress != "" {
			mustStartQueries = append(mustStartQueries, nestedTagQuery)
		}

		// Create the final bool query for finding the correct starting spans
		startBoolMustQuery := elastic.NewBoolQuery().Must(mustStartQueries...)

		/****** End span querying *****/
		var mustEndQueries []elastic.Query

		// Find the spans having the correct service and operation name
		endServiceTermQuery := elastic.NewTermQuery("process.serviceName", endService)
		endOperationTermQuery := elastic.NewTermQuery("operationName", endOperation)

		// Add the queries to the must clause of the upcoming bool query
		mustEndQueries = append(mustEndQueries, endServiceTermQuery)
		mustEndQueries = append(mustEndQueries, endOperationTermQuery)

		// Only search for new spans if this is not the first run
		if lastSuccessfulTraceTimestamp > 0 {
			mustEndQueries = append(mustEndQueries, rangeQuery)
		}

		// Search on the operation tag fiels if value is given
		if ipAddress != "" {
			mustEndQueries = append(mustEndQueries, nestedTagQuery)
		}

		// Create the final bool query for finding the correct ending spans
		endBoolMustQuery := elastic.NewBoolQuery().Must(mustEndQueries...)

		// Top bool query to search for the matching start and end spans
		upperBoolQuery := elastic.NewBoolQuery().Should(startBoolMustQuery, endBoolMustQuery)

		resultMap := map[string]*resultMapping{}
		scrollq := client.Scroll("jaeger-span*").Query(upperBoolQuery)

		// Do a scroll to get all results
		for {
			results, err := scrollq.Do(ctx)
			if err == io.EOF {
				break
			}
			if err != nil {
				panic(err)
			}

			// Loop over all hits for this scroll
			for _, hit := range results.Hits.Hits {

				// Deserialize hit.Source into a span
				var span JaegerSpan
				err := json.Unmarshal(hit.Source, &span)
				if err != nil {
					panic(err)
				}

				// The endtime of a span is not returned by Jaeger. We calculate this manually in order to find the most relevant trace (see below)
				span.EndTime = span.StartTime + span.Duration

				// Keep track of spans per trace ID
				if _, ok := resultMap[span.TraceID]; !ok {
					resultMap[span.TraceID] = &resultMapping{}
				}

				// Check if start or end span
				if span.OperationName == startOperation && span.Process.ServiceName == startService {
					resultMap[span.TraceID].startResults = append(resultMap[span.TraceID].startResults, span)
				}

				if span.OperationName == endOperation && span.Process.ServiceName == endService {
					resultMap[span.TraceID].endResults = append(resultMap[span.TraceID].endResults, span)
				}
			}
		}

		for _, result := range resultMap {

			// If we only have matched start or end spans, do nothing as this is not the trace we want.
			if len(result.endResults) == 0 {
				continue
			}

			if len(result.startResults) == 0 {
				continue
			}

			// Sort the matched spans. This is important as some services and operations may be called multiple times in a single trace.
			// We would therefore like to know find the start span with the lowest start time and end span with the highest end time as this is the upper bound of service + operation combos that we would like to match
			sort.Sort(result.startResults)
			sort.Sort(result.endResults)

			// Calculate the latency
			firstStartTime := result.startResults[0].StartTime
			lastEndTime := result.endResults[len(result.endResults)-1].StartTime
			lastDuration := result.endResults[len(result.endResults)-1].Duration

			latencyNanoseconds := time.Duration((lastEndTime - firstStartTime + lastDuration) * 1000)

			fmt.Printf("Got latency of %s \n", latencyNanoseconds.String())

			// Check the latest start timestamp for which we have successfully extracted traces. This will become the lower bound for the follow elasticsearch query
			if firstStartTime > uint64(lastSuccessfulTraceTimestamp) {
				lastSuccessfulTraceTimestamp = int(firstStartTime)
			}
		}

		fmt.Println("Done with latency export")
		fmt.Println()
	}

}

type resultMapping struct {
	startResults jaegerStartSpanResults
	endResults   jaegerEndSpanResults
}

type jaegerStartSpanResults []JaegerSpan

func (u jaegerStartSpanResults) Len() int {
	return len(u)
}
func (u jaegerStartSpanResults) Swap(i, j int) {
	u[i], u[j] = u[j], u[i]
}
func (u jaegerStartSpanResults) Less(i, j int) bool {
	return u[i].StartTime < u[j].StartTime
}

type jaegerEndSpanResults []JaegerSpan

func (u jaegerEndSpanResults) Len() int {
	return len(u)
}
func (u jaegerEndSpanResults) Swap(i, j int) {
	u[i], u[j] = u[j], u[i]
}
func (u jaegerEndSpanResults) Less(i, j int) bool {
	return u[i].EndTime < u[j].EndTime
}
